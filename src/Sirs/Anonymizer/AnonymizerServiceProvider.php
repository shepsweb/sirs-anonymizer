<?php

namespace Sirs\Anonymizer;

use Illuminate\Support\ServiceProvider;

class AnonymizerServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the Tasks services.
     */
    public function boot(): void
    {
        // Publish the config file
        $this->publishes([
            __DIR__.'/config.php' => config_path('anonymizer.php'),
        ], 'config');
    }

    /**
     * Register the application services.
     */
    public function register(): void
    {
        // register the command
        $this->commands([
            AnonymizeData::class
        ]);
    }
}