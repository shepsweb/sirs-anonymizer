<?php

namespace Sirs\Anonymizer;

use Faker\Generator;
use InvalidArgumentException;
use Exception;
use Illuminate\Database\QueryException;
use PDOException;
use Illuminate\Support\Facades\Schema;

trait AnonymizerTrait
{
    protected Generator $faker;

    public function anonymize(): void
    {
        $this->attributes = $this->anonymizeAttributes($this->attributes);
        try {
            $this->save();
        } catch (PDOException | QueryException $e) {
            if (str_contains($e->getMessage(), 'NOT NULL') || str_contains($e->getMessage(), 'null')) {
                throw new Exception(
                    "Anonymizer Error: Failed to save due to null constraint violation. Check your anonymize array configuration."
                );
            }
            throw $e;
        }
    }

    protected function anonymizeAttributes(array $attributes): array
    {
        $this->getFaker();
        
        foreach ($this->anonymize as $field => $type) {
            try {
                $attributes[$field] = $this->faker->$type;
            } catch (InvalidArgumentException $e) {
                $func = 'fake'.ucfirst($type);
                if (method_exists($this, $func)) {
                    $attributes[$field] = $this->$func();
                } elseif ($type === null) {
                    try {
                        // Check if column exists
                        if (!Schema::hasColumn($this->getTable(), $field)) {
                            throw new Exception("Column '{$field}' doesn't exist in table '{$this->getTable()}'");
                        }

                        // Get column information using Laravel 11's schema builder
                        $columns = Schema::getConnection()
                            ->getSchemaBuilder()
                            ->getColumns($this->getTable());

                        $column = collect($columns)->first(fn($col) => $col['name'] === $field);

                        if ($column && $column['nullable']) {
                            $attributes[$field] = null;
                        } else {
                            throw new Exception("Column '{$field}' is not nullable");
                        }
                    } catch (Exception $e) {
                        throw new Exception(
                            'Anonymizer Error: Check anonymize array on model. Unable to set non-nullable field to null for '.$field.
                            ' ('.$e->getMessage().')'
                        );
                    }
                } else {
                    throw new InvalidArgumentException(
                        'Unknown anonymizer function '.$func.' for type '.$type
                    );
                }
            }
        }

        return $attributes;
    }

    protected function fakeFirstName(): string
    {
        return $this->faker->firstName;
    }

    protected function fakeLastName(): string
    {
        return $this->faker->lastName;
    }

    protected function fakeDob(): string
    {
        return $this->faker->date('Y-m-d', '1990-01-01');
    }

    protected function fakeRandomNumber(?int $digits = null): int
    {
        return $this->faker->randomNumber($digits);
    }

    protected function fakeSlug(): string
    {
        return implode('-', $this->faker->unique->words(2));
    }

    protected function getFaker(): void
    {
        $this->faker = \Faker\Factory::create();
    }
}