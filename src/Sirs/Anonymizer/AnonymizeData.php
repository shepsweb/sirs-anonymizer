<?php

namespace Sirs\Anonymizer;

use Illuminate\Console\Command;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\DB;
use ReflectionClass;
use ReflectionMethod;
use InvalidArgumentException;

class AnonymizeData extends Command
{
    protected $signature = 'anonymizer:anonymize';

    protected $description = 'Anonymizes data for communication channels and models that use the AnonymizeTrait trait';

    public function handle(): int
    {
        // Critical safety check for production environment
        if (App::environment('production')) {
            $this->error('!!!!!!!!!!!!!!WARNING!!!!!!!!!!!!!!');
            $this->error('PRODUCTION ENVIRONMENT DETECTED');
            if (!$this->confirm('This will anonymize production data. Are you absolutely sure you want to continue?')) {
                $this->error('Operation cancelled. No data was modified.');
                return Command::FAILURE;
            }
        }

        try {
            $this->anonymizeModels();
            $this->truncateModelsAndTables();
            
            $this->info('Anonymization completed successfully.');
            return Command::SUCCESS;
        } catch (\Exception $e) {
            $this->error('An error occurred during anonymization: ' . $e->getMessage());
            return Command::FAILURE;
        }
    }

    protected function anonymizeModels(): void
    {
        $classes = config('anonymizer.models', []);

        foreach ($classes as $modelClass) {
            if (!class_exists($modelClass)) {
                $this->error("Model class {$modelClass} does not exist. Skipping.");
                continue;
            }

            if (!$this->hasAnonymizeMethod($modelClass)) {
                $this->error("{$modelClass} does not have a public anonymize method. Skipping.");
                continue;
            }

            $this->info("Anonymizing {$modelClass}");
            $this->processModelBatch($modelClass);
        }
    }

    protected function processModelBatch(string $modelClass): void
    {
        $softDeleteCount = $this->countSoftDeletes($modelClass);
        $totalItems = $modelClass::count() + $softDeleteCount;
        $processed = 0;

        $progressBar = $this->output->createProgressBar($totalItems);

        while ($processed < $totalItems) {
            /** @var Model $query */
            $query = $softDeleteCount > 0 
                ? $modelClass::withTrashed()->skip($processed)
                : $modelClass::skip($processed);

            $items = $query->take(1000)->get();

            $items->each(function ($item) use ($progressBar) {
                try {
                    $item->anonymize();
                    $progressBar->advance();
                } catch (\Exception $e) {
                    $this->error("Failed to anonymize {$item->id}: " . $e->getMessage());
                }
            });

            $processed += $items->count();
        }

        $progressBar->finish();
        $this->newLine();
    }

    protected function truncateModelsAndTables(): void
    {
        // Truncate models
        $truncateClasses = config('anonymizer.truncate.models', []);
        foreach ($truncateClasses as $truncateClass) {
            if (!class_exists($truncateClass)) {
                $this->error("Model class {$truncateClass} does not exist. Skipping truncate.");
                continue;
            }

            $this->info("Truncating model {$truncateClass}");
            try {
                $truncateClass::truncate();
            } catch (\Exception $e) {
                $this->error("Failed to truncate model {$truncateClass}: " . $e->getMessage());
                throw $e;
            }
        }

        // Truncate tables
        $truncateTables = config('anonymizer.truncate.tables', []);
        foreach ($truncateTables as $truncateTable) {
            $this->info("Truncating table {$truncateTable}");
            try {
                if (!DB::getSchemaBuilder()->hasTable($truncateTable)) {
                    $this->error("Table {$truncateTable} does not exist. Skipping truncate.");
                    continue;
                }
                DB::table($truncateTable)->truncate();
            } catch (\Exception $e) {
                $this->error("Failed to truncate table {$truncateTable}: " . $e->getMessage());
                throw $e;
            }
        }
    }

    protected function hasAnonymizeMethod(string $class): bool
    {
        try {
            $reflection = new ReflectionClass($class);
            foreach ($reflection->getMethods(ReflectionMethod::IS_PUBLIC) as $method) {
                if ($method->name === 'anonymize') {
                    return true;
                }
            }
            return false;
        } catch (\ReflectionException $e) {
            return false;
        }
    }

    protected function countSoftDeletes(string $class): int
    {
        try {
            if (in_array('Illuminate\Database\Eloquent\SoftDeletes', class_uses_recursive($class), true)) {
                return $class::onlyTrashed()->count();
            }
            return 0;
        } catch (\Exception $e) {
            $this->error("Error checking soft deletes for {$class}: " . $e->getMessage());
            return 0;
        }
    }
}