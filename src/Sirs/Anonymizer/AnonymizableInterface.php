<?php

namespace Sirs\Anonymizer;

/**
 * Defines interface for an anonymizable class
 */
interface AnonymizableInterface
{
    /**
     * Anonymize the current model instance
     *
     * @return void
     */
    public function anonymize(): void;
}