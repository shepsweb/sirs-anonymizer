<?php

namespace Sirs\Anonymizer\Tests\Models;

use Illuminate\Database\Eloquent\Model as IlluminateModel;

class Participant extends IlluminateModel
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'participant_test';

    public $timestamps = false;

    protected $guarded = [];

    protected $fillable = ['name'];

    public function anonymize()
    {
        $this->attributes['name'] = 'fake value';
        $this->save();
    }
}
