<?php

namespace Sirs\Anonymizer\Tests\Models;

use Illuminate\Database\Eloquent\Model as IlluminateModel;
use Illuminate\Database\Eloquent\SoftDeletes;
use Sirs\Anonymizer\AnonymizableInterface;
use Sirs\Anonymizer\AnonymizerTrait;

class User extends IlluminateModel implements AnonymizableInterface
{
    use SoftDeletes, 
        AnonymizerTrait;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'user_test';

    protected $guarded = [];

    protected $fillable = ['first_name', 'last_name', 'dob', 'notes', 'custom_type'];

    protected $anonymize = [
        'first_name' => 'firstName', 
        'last_name' => 'lastName', 
        'dob' => 'dob', 
        'notes' => null, 
        'medNo' => 'randomNumber',
        'slug' =>'slug',
        'custom_type' => 'customType'
    ];

    protected function fakeCustomType()
    {
        return 'I was created with the fakeCustomType function';
    }

}
