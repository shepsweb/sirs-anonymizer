<?php

namespace Sirs\Anonymizer\Tests\Models;

use Illuminate\Database\Eloquent\Model as IlluminateModel;
use Sirs\Anonymizer\AnonymizableInterface;
use Sirs\Anonymizer\AnonymizerTrait;

class Clinic extends IlluminateModel implements AnonymizableInterface
{
    use AnonymizerTrait;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'clinic_test';

    protected $guarded = [];

    protected $fillable = ['name'];

    protected $anonymize = [
        'name' => 'undefinedCustomType'
    ];
}
