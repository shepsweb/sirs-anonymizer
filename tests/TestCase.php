<?php

namespace Sirs\Anonymizer\Tests;

use Orchestra\Testbench\TestCase as OrchestraTestCase;
use Sirs\Anonymizer\AnonymizerServiceProvider;

class TestCase extends OrchestraTestCase
{
    public function setUp(): void
    {
        parent::setUp();
        $this->loadMigrationsFrom(__DIR__.'/files/migrations');
        $this->artisan('migrate', ['--database' => 'testbench'])->run();
    }

    public function invokeMethod(&$object, $methodName, array $parameters = array())
    {
        $reflection = new \ReflectionClass(get_class($object));
        $method = $reflection->getMethod($methodName);
        $method->setAccessible(true);

        return $method->invokeArgs($object, $parameters);
    }

    protected function getPackageProviders($app)
    {
        return [
            AnonymizerServiceProvider::class,
        ];
    }

    protected function getEnvironmentSetUp($app)
    {
        $app['config']->set('database.default', 'testbench');
        $app['config']->set('database.connections.testbench', [
            'driver'   => 'sqlite',
            'database' => ':memory:',
            'prefix'   => '',
        ]);
        $app['config']->set('sluggable', [
            'source' => null,
            'maxLength' => null,
            'maxLengthKeepWords' => true,
            'method' => null,
            'separator' => '-',
            'unique' => true,
            'uniqueSuffix' => null,
            'includeTrashed' => false,
            'reserved' => null,
            'onUpdate' => false,
        ]);
        $app['config']->set('anonymizer', [
            'models' => [],
            'truncate' => [
                'models' => [],
                'tables' => []
            ]
        ]);
    }
}
