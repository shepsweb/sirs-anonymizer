<?php

namespace Sirs\Anonymizer\Tests\Unit;

use Sirs\Anonymizer\Tests\TestCase;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;
use Sirs\Anonymizer\Tests\Models\User;
use Sirs\Anonymizer\Tests\Models\Participant;

class AnonymizeTest extends TestCase
{
  	public function setUp() : void
	{
		parent::setUp();

		$this->users = [
		    ['first_name' => 'FakeFirstOne', 'last_name' => 'FakeLastOne', 'dob' => '1991-01-01', 'notes' => 'This is a note.', 'medNo' => 0, 'slug' => 'fake-slug', 'custom_type' => 'hi', 'deleted_at' => null],
		    ['first_name' => 'FakeFirstTwo', 'last_name' => 'FakeLastTwo', 'dob' => '1991-09-23', 'notes' => 'I belong to a soft deleted record!', 'medNo' => 0, 'slug' => 'fake-slug', 'custom_type' => 'hello', 'deleted_at' => '2020-01-01']
		];
		DB::table('user_test')->insert($this->users);
		DB::table('participant_test')->insert(['name' => 'some string']);
	}

	/** @test */
	function a_model_is_anonymized_with_the_anonymize_trait_function()
	{	
		Config::set('anonymizer.models', [\Sirs\Anonymizer\Tests\Models\User::class]);	
		$this->artisan('anonymizer:anonymize')->run();

		$results = DB::table('user_test')->get();

		// built in types
		$this->assertNotEquals($results[0]->first_name, $this->users[0]['first_name']);
		$this->assertNotEquals($results[0]->last_name, $this->users[0]['last_name']);
		$this->assertNotEquals($results[0]->dob, $this->users[0]['dob']);
		$this->assertLessThanOrEqual('1990-01-01', $results[0]->dob);
		$this->assertNull($results[0]->notes);
		$this->assertNotEquals($results[0]->slug, $this->users[0]['slug']);
		$this->assertNotEquals($results[0]->medNo, $this->users[0]['medNo']);

		// custom type
		$this->assertEquals($results[0]->custom_type, 'I was created with the fakeCustomType function');

		// soft deleted record
		$this->assertNotEquals($results[1], $this->users[1]);
	}

	/** @test */
	function a_model_is_anonymized_with_the_anonymize_model_function()
	{
		Config::set('anonymizer.models', [\Sirs\Anonymizer\Tests\Models\Participant::class]);	
		$this->artisan('anonymizer:anonymize')->run();

		$results = DB::table('participant_test')->get();
		$this->assertEquals($results[0]->name, 'fake value');
	}

	/** @test */
	function a_model_is_not_anonymized_when_missing_the_anonymize_model_function()
	{
		Config::set('anonymizer.models', [\Sirs\Anonymizer\Tests\Models\Study::class]);	
		$this->artisan('anonymizer:anonymize')
			->expectsOutput('Sirs\Anonymizer\Tests\Models\Study does not have a public anonymize method. Skipping.')
			->assertExitCode(0);
	}

    /** @test */
	function an_undefined_custom_anonymize_type_produces_error()
	{
	    DB::table('clinic_test')->insert(['name' => 'clinic name']);
	    Config::set('anonymizer.models', [\Sirs\Anonymizer\Tests\Models\Clinic::class]);    

	    $this->artisan('anonymizer:anonymize')
	        ->expectsOutput('Failed to anonymize 1: Unknown anonymizer function fakeUndefinedCustomType for type undefinedCustomType')
	        ->assertSuccessful();
	        
	    // Verify data wasn't changed
	    $result = DB::table('clinic_test')->first();
	    $this->assertEquals('clinic name', $result->name);
	}

    /** @test */
    function a_warning_is_shown_in_production()
    {
        App::shouldReceive('environment')->andReturn('production');
        $this->artisan('anonymizer:anonymize')
            ->expectsQuestion('This will anonymize production data. Are you absolutely sure you want to continue?', false)
            ->expectsOutput('Operation cancelled. No data was modified.')
            ->assertExitCode(Command::FAILURE);
    }

    /** @test */
    function a_user_can_abort_anonymizing_in_production()
    {
        App::shouldReceive('environment')->andReturn('production');
        Config::set('anonymizer.models', [\Sirs\Anonymizer\Tests\Models\Participant::class]);
        $this->artisan('anonymizer:anonymize')
            ->expectsQuestion('This will anonymize production data. Are you absolutely sure you want to continue?', false)
            ->expectsOutput('Operation cancelled. No data was modified.')
            ->assertExitCode(Command::FAILURE);
        
        $results = DB::table('participant_test')->get();
        $this->assertEquals($results[0]->name, 'some string');
    }
}
