<?php

namespace Sirs\Anonymizer\Tests\Unit;

use Sirs\Anonymizer\Tests\TestCase;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\QueryException;

class TruncateTest extends TestCase
{
  	public function setUp() : void
	{
		parent::setUp();

		DB::table('revisions')->insert(['name' => 'Marc']);
		DB::table('user_test')->insert(['first_name' => 'FakeFirstOne', 'last_name' => 'FakeLastOne', 'dob' => '1991-01-01', 'notes' => 'This is a note.', 'medNo' => 0, 'slug' => 'fake-slug', 'custom_type' => 'hi', 'deleted_at' => null]);
	}

	/** @test */
	function a_table_is_truncated()
	{	
		Config::set('anonymizer.truncate.tables', ['revisions']);
		$this->artisan('anonymizer:anonymize')->run();

		$results = DB::table('revisions')->get();
		$this->assertEmpty($results);
	}

	/** @test */
	function a_model_is_truncated()
	{
		Config::set('anonymizer.truncate.models', [\Sirs\Anonymizer\Tests\Models\User::class]);
		$this->artisan('anonymizer:anonymize')->run();

		$results = DB::table('user_test')->get();
		$this->assertEmpty($results);
	}

    /** @test */
    function an_incorrect_table_name_shows_error()    // Changed from throws_exception_during_truncate
    {    
        Config::set('anonymizer.truncate.tables', ['missing_table']);
        
        $this->artisan('anonymizer:anonymize')
            ->expectsOutput('Table missing_table does not exist. Skipping truncate.')
            ->assertSuccessful();
    }

    /** @test */
    function an_incorrect_model_name_shows_error()    // Changed from throws_exception_during_truncate
    {    
        Config::set('anonymizer.truncate.models', ['Sirs\\Anonymizer\\Tests\\Models\\MissingModel']);
        
        $this->artisan('anonymizer:anonymize')
            ->expectsOutput('Model class Sirs\Anonymizer\Tests\Models\MissingModel does not exist. Skipping truncate.')
            ->assertSuccessful();
    }
}
