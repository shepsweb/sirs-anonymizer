# Sirs-Anonymizer #
Package for defining data to be anonymized on models and functionality to anonymize it. Package can also truncate any models/tables specified by config.
## Contents ##
* AnonymizableInterface - interface defining public API of class that can be anonymized.
* AnonymizerTrait - Trait with anonymize method.  Use of this trait on Eloquent model satisfies AnonymizableInterface.
* AnonymizeData - Artisan console command that calls the anonymize method to anonymize each model listed in config/anonymizer.php > models and truncates each table/model listed in config/anonymizer.php > truncate.
## Installation & Configuration ##
Install via Composer:
```bash
composer require sirs/anonymizer
```

The package uses Laravel's package auto-discovery, so the service provider will be automatically registered. However, if you're using Laravel without auto-discovery, add the service provider to config/app.php:
```php
    ...
    Sirs\Anonymizer\AnonymizerServiceProvider::class,
    ...
```
Run `php artisan vendor:publish --provider="Sirs\Anonymizer\AnonymizerServiceProvider"`
Add the models to be anonymized and models/tables to be truncated to config/anonymizer.php:
```php
...
    'models'=>[
        // Class names here
        App\Models\Participant::class,
        Sirs\Communications\Models\Channel::class,
    ],
    'truncate'=>[
        'models'=>[
            // Class names here
            App\Models\CommLog::class
        ],
        'tables'=>[
            // Table names here
            'rsp_example_1'
        ]
    ],
...
```
Add the interface and trait to each model you want to anonymize and define the fields you would like to anonymize. Model creation is required for any model-less table you want to anonymize.
```php
use Sirs\Anonymizer\AnonymizableInterface;
use Sirs\Anonymizer\AnonymizerTrait;

class Participant extends Model implements AnonymizableInterface 
{
    use AnonymizerTrait;
    
    protected $anonymize = [
        // field_name => type
        'first_name' => 'firstName',
        'last_name' => 'lastName',
        'dob' => 'dob',
        'mrn' => 'randomNumber',
        'medrecord' => null,
    ];
```
Defining a field as null will attempt to nullify that value. If the table column is not nullable, the anonymization of that model will fail with feedback on which field caused the error.
## Usage ##
To anonymize all models and truncate all models/tables listed in config/anonymizer.php run
```bash
php artisan anonymizer:anonymize
```
Out of the box you can use any Faker formatter name (i.e. name, firstName, numberBetween) as a type. At this point you CANNOT specify arguments for those formatters that accept them. 
## Built in types ##
* firstName - returns a first name string e.g. "Charlie"
* lastName - returns a last name string e.g. "Smith"
* dob - returns a date string with format Y-m-d before 1990-01-01
* randomNumber - returns a random number
* slug - returns a kebab cased two word string
### Creating custom types ###
You can create custom types by adding fakeTypeName methods to your class. For example, if you needed to de-identify a phone number that was an attribute on the Participant model you could add the following method:
```php
protected function fakePhoneNumber()
{
    return $this->faker->phoneNumber;
}
```
Note that $this->faker is created by methods used during anonymization and will be available when your custom type method is called.